import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastaConfig, ToastaService } from 'ngx-toasta';
import { DatePtBRService } from '../date-pt-br/date-pt-br.service';
import { Evento } from './../shared/evento.model';
import { EventoService } from './evento.service';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {
  closeResult: string;
  listaDeEventos: Array<Evento>;
  evento: Evento;
  modalTitleContent: string;
  modalBodyContent: string;
  @ViewChild('modalConfirm') modalConfirm: ElementRef;

  constructor(
    private eventoService: EventoService,
    private datePtBRService: DatePtBRService,
    private modalService: NgbModal,
    private toastaService: ToastaService,
    private toastaConfig: ToastaConfig) {
    this.toastaConfig.theme = 'default';
  }

  ngOnInit() {
    this.evento = new Evento();
    this.resetForm();
    this.getListaDeEventos();
  }


  buscarEventos(busca: any) {
    this.eventoService.getEventos(busca.busca, busca.coluna).subscribe(
      (eventos: Array<Evento> ) => this.listaDeEventos = eventos
    );
  }

  getListaDeEventos() {
    this.eventoService.getEventos().subscribe(
      (eventos: Array<Evento> ) => this.listaDeEventos = eventos
    );
  }

  resetForm(form?: NgForm) {
    this.evento = new Evento();
    if (form !== null && form !== undefined) {
      form.reset();
    }
  }

  OnSubmit(form: NgForm) {
    if (form.invalid) {
        return;
    }

    try {
      let erros: string;
      if (this.evento.ID !== null && this.evento.ID !== undefined && this.evento.ID > 0 ) {
        this.eventoService.editEvento( this.cloneEventoFormatadoUS(this.evento)).subscribe(
          (data: any) => {
            this.toastaService.success('Evento ID(' + this.evento.ID + ') atualizado com sucesso.');
            this.modalService.dismissAll();
            this.resetForm(form);
            this.getListaDeEventos();
          },
          (error: any) => {
            erros = '';
            if (error.error.Message !== undefined) {
              for (const erro of JSON.parse(error.error.Message)) {
                erros += '\n<br/> - ' + erro;
              }
            }
            this.toastaService.error('Não foi possível gravar, tente novamente.' + erros);
          }
        );
      } else {
        this.eventoService.addEvento(this.cloneEventoFormatadoUS(this.evento)).subscribe(
          (data: any) => {
            if (data.ID !== null && data.ID !== 0) {
              this.toastaService.success('Evento ID(' + data.ID + ') gravado com sucesso');
              this.modalService.dismissAll();
              this.resetForm(form);
              this.getListaDeEventos();
            } else {
              this.toastaService.error('Não foi possível gravar, tente novamente');
            }
          },
          (error: any) => {
            erros = '';
            if (error.error.Message !== undefined) {
              for (const erro of JSON.parse(error.error.Message)) {
                erros += '\n<br/> - ' + erro;
              }
            }
            this.toastaService.error('Não foi possível gravar, tente novamente.' + erros);
          }
        );
      }
    } catch (error) {
      this.toastaService.error('Não foi possível gravar, tente novamente' + error);
    }
  }

  deleteEvento(ID: Number) {
    try {
      this.eventoService.deleteEvento(ID).subscribe(
        (data: any) => {
          this.toastaService.success('Evento ID ' + ID + ' excluído com sucesso.');
          this.getListaDeEventos();
        },
        (error: any) => { this.toastaService.error('Não foi possível excluir o evento ID ' + ID + ', tente novamente.'); }
      );
    } catch (error) {
      this.toastaService.error('Não foi possível excluir o evento ID ' + ID + ', tente novamente' + error);
    }
  }

  private cloneEventoFormatadoPtBR(evento: Evento): Evento {
    const eFormatado: Evento = new Evento();
    eFormatado.ID = evento.ID;
    eFormatado.DataCadastro = evento.DataCadastro;
    eFormatado.DataEvento = this.datePtBRService.paraPtBR(evento.DataEvento);
    eFormatado.QuantidadeDePessoas = evento.QuantidadeDePessoas;
    eFormatado.Responsavel = evento.Responsavel;
    eFormatado.Descricao = evento.Descricao;
    return eFormatado;
  }

  private cloneEventoFormatadoUS(evento: Evento): Evento {
    const eFormatado: Evento = new Evento();
    eFormatado.ID = evento.ID;
    eFormatado.DataCadastro = evento.DataCadastro;
    eFormatado.DataEvento = this.datePtBRService.paraUS(evento.DataEvento);
    eFormatado.QuantidadeDePessoas = evento.QuantidadeDePessoas;
    eFormatado.Responsavel = evento.Responsavel;
    eFormatado.Descricao = evento.Descricao;
    return eFormatado;
  }

  editModal(evento: Evento, content, form: NgForm) {
    this.resetForm(form);
    this.evento = this.cloneEventoFormatadoPtBR(evento);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  deleteModal(evento: Evento) {
    this.modalTitleContent = 'Atenção!';
    this.modalBodyContent = 'Tem certeza que deseja excluir o evento ID(' + evento.ID + ') "' + evento.Descricao + '"';
    this.modalService.open(this.modalConfirm,
      {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        if (result === 'sim' ) {
          this.deleteEvento(evento.ID);
        }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  newModal(content, form: NgForm) {
    this.resetForm(form);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
