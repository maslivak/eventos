import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Evento } from '../shared/evento.model';

@Injectable({
  providedIn: 'root'
})
export class EventoService {
  readonly rootUrl = 'http://localhost:57831';

  constructor(private http: HttpClient) {}

  // private dataSoNumerosParaDate(d: string) {
  //   if (d == null || d === undefined) {
  //     return '';
  //   }
  //   return d.substr(4, 4) + '-' + d.substr(2, 2) + '-' + d.substr(0, 2);
  // }

  addEvento(evento: Evento) {
    const body: Evento = {
      ID: null,
      DataCadastro: null,
      DataEvento: evento.DataEvento,
      QuantidadeDePessoas: evento.QuantidadeDePessoas,
      Responsavel: evento.Responsavel,
      Descricao: evento.Descricao
    };
    return this.http.post(this.rootUrl + '/api/eventos', body);
  }

  editEvento(evento: Evento) {
    const body: Evento = evento;
    return this.http.put(this.rootUrl + '/api/eventos/' + evento.ID, body);
  }

  deleteEvento(ID: Number) {
    return this.http.delete(this.rootUrl + '/api/eventos/' + ID);
  }

  getEventos(busca?: string, coluna?: number): Observable<Evento[]> {
    if (busca !== null && busca !== undefined && busca !== '') {
      return this.http.get<Evento[]>(this.rootUrl + '/api/eventos/?busca=' + busca + '&coluna=' + coluna)
      .pipe(catchError(this.handleError<Evento[]>('getEventos', [])));
    }

    return this.http.get<Evento[]>(this.rootUrl + '/api/eventos')
    .pipe(catchError(this.handleError<Evento[]>('getEventos', [])));
  }

  private handleError<T> (operation = 'operation', result?: T,  erros?: Observable<string>) {
    if (erros !== null && erros !== undefined) {
      erros.forEach(erro => {
        console.error(erro); // log to console instead
      });
    }

    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
