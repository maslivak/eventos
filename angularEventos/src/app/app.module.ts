import { EventoService } from './evento/evento.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EventoComponent } from './evento/evento.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskModule } from 'ngx-mask';
import { TabelaResponsivaComponent } from './tabela-responsiva/tabela-responsiva.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ToastaModule} from 'ngx-toasta';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    EventoComponent,
    TabelaResponsivaComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    NgbModule,
    ToastaModule.forRoot()
  ],
  providers: [EventoService],
  bootstrap: [AppComponent]
})
export class AppModule { }

