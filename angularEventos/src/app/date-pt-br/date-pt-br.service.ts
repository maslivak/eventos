import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DatePtBRService {
  constructor() { }

  public readonly formatoDataPtBRComHora: string = 'DD/MM/YYYY HH:mm:ss';
  public readonly formatoDataUSComHora: string = 'YYYY-MM-DD HH:mm:ss';

  public readonly formatoDataPtBR: string = 'DD/MM/YYYY';
  public readonly formatoDataUS: string = 'YYYY-MM-DD';

  public paraPtBR(dataUS: string, comHora: boolean = true): string {
    let formatoSaida: string = this.formatoDataPtBRComHora;
    if (comHora === false) {
      formatoSaida = this.formatoDataPtBR;
    }
    const d: moment.Moment = moment(dataUS, this.formatoDataUSComHora);
    if (d.isValid()) {
      return moment(d).format(formatoSaida);
    } else {
      return '';
    }
  }

  public paraUS(dataPtBR: string, comHora: Boolean = true): string {
    let formatoSaida: string = this.formatoDataUSComHora;
    if (comHora === false) {
      formatoSaida = this.formatoDataUS;
    }
    const d: moment.Moment = moment(dataPtBR, this.formatoDataPtBRComHora);
    if (d.isValid()) {
      return moment(d).format(formatoSaida);
    } else {
      return '';
    }
  }

}


