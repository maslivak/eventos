import { TestBed } from '@angular/core/testing';

import { DatePtBRService } from './date-pt-br.service';

describe('DatePtBRService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatePtBRService = TestBed.get(DatePtBRService);
    expect(service).toBeTruthy();
  });
});
