import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaResponsivaComponent } from './tabela-responsiva.component';

describe('TabelaResponsivaComponent', () => {
  let component: TabelaResponsivaComponent;
  let fixture: ComponentFixture<TabelaResponsivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaResponsivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaResponsivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
