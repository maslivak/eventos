import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Evento } from '../shared/evento.model';

@Component({
  selector: 'app-tabela-responsiva',
  templateUrl: './tabela-responsiva.component.html',
  styleUrls: ['./tabela-responsiva.component.css']
})
export class TabelaResponsivaComponent implements OnInit {
  @Input() listaDeEventos: Array<Evento>;
  @Output() addEvento = new EventEmitter();
  @Output() editEvento = new EventEmitter<Evento>();
  @Output() deleteEvento = new EventEmitter<Number>();
  @Output() buscarEventos = new EventEmitter<any>();
  @ViewChild('campoBusca') campoBusca: ElementRef;

  public listaDeColunasBuscar: Array<string>;
  public colunaBuscar: number;
  public textoBuscar: string;

  constructor() {
  }

  tipoBusca(index: number) {
    if (index < 0) {
      this.colunaBuscar = -1;
      this.campoBusca.nativeElement.placeholder = 'Buscar...';
      return;
    }
    this.colunaBuscar = index;
    this.campoBusca.nativeElement.placeholder = 'Buscar por ' + this.listaDeColunasBuscar[index] + '...';
  }

  callAddEvento() {
    this.addEvento.emit();
  }

  callDeleteEvento(ID: Number) {
    this.deleteEvento.emit(ID);
  }

  callEditEvento(evento: Evento) {
    this.editEvento.emit(evento);
  }

  buscaComEnter(keyEvent: any) {
    if (keyEvent.which === 13) {
      this.callSearchEventos();
    }
  }

  callLimparBusca() {
    this.textoBuscar = '';
    this.campoBusca.nativeElement.value = '';
    this.colunaBuscar = -1;
    this.callSearchEventos();
  }

  callSearchEventos() {
    this.textoBuscar = this.campoBusca.nativeElement.value;
    this.buscarEventos.emit({
      busca: this.textoBuscar,
      coluna: this.colunaBuscar
     });
  }

  ngOnInit() {
    this.listaDeColunasBuscar = new Array<string>();
    this.colunaBuscar = -1;
    this.textoBuscar = '';
    this.campoBusca.nativeElement.placeholder = 'Buscar...';
    this.listaDeColunasBuscar[0] = 'ID';
    this.listaDeColunasBuscar[1] = 'Data de Cadastro';
    this.listaDeColunasBuscar[2] = 'Data do Evento';
    this.listaDeColunasBuscar[3] = 'Qtde. Pessoas';
    this.listaDeColunasBuscar[4] = 'Responsável';
    this.listaDeColunasBuscar[5] = 'Descrição';
  }

}
