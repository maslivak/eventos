export class Evento {
  ID?: number;
  DataCadastro?: string;
  DataEvento?: string;
  QuantidadeDePessoas?: number;
  Responsavel?: string;
  Descricao?: string;
}
