# eventos

Teste com C# Web API + Angular 6

CADASTRO DE EVENTOS - (ANGULAR + C# WEB API)
Instruções para execução:

   - O banco de dados está incorporado ao projeto dentro da pasta 
     APP_Data\dbLocal.mdf
	 
   - Primeiro iniciar a depuração do projeto no Visual Studio,
     Build->Clean Solution, Build->Rebuild Solution e F5 para iniciar
	 a depuração.
	 
   - Certifique que a porta que está rodando o IIS Express é a mesma 
     informada na varíavel "rootUrl = 'http://localhost:57831';", 
	 que está dentro do arquivo 
	 "[workspace]\angularEventos\src\app\evento\evento.service.ts".

   - Instalar o Visual Code:
     https://code.visualstudio.com/download

   - Intalar o NodeJS:
     https://nodejs.org/en/download/

   - Instalar o Angular CLI, no terminal executa:
     npm install -g @angular/cli
 
   - Abrir o pasta do projeto angular evento no Visual Code, 
     depois no terminal executar "npm install", depois "ng serve".
	 
   - Depois clique no link abaixo (se não foi alterada a porta 
     padrão do angular 4200).
	 
   - Se a porta do angular não for a 4200, será necessário atualizar o CORS
     do visual Studio em "[workspace]\eventos\eventos\App_Start\WebApiConfig.cs",
     para o novo endereço/porta em 
	 "config.EnableCors(new EnableCorsAttribute("http://localhost:4200", headers: "*", methods: "*"));"
