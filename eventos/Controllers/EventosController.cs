﻿using AutoMapper;
using eventos.DAL;
using eventos.Models;
using eventos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace eventos.Controllers
{
    public class EventosController : ApiController
    {
        private EventoContext db = new EventoContext();

        // GET: api/Eventos
        public IQueryable<Evento> GetEventos()
        {
            return db.Eventos;
        }

        // GET: api/Eventos/busca/coluna
        public IQueryable<Evento> GetEventos(string busca, int coluna = -1)
        {
            int numeroBuscar = -1;
            DateTime dataIni = default(DateTime);
            DateTime dataFim = default(DateTime);
            var retorno = db.Eventos;
            switch (coluna)
            {
                case 0:
                    if (int.TryParse(busca, out numeroBuscar))
                        return (DbQuery<Evento>)retorno.Where(c => c.ID == numeroBuscar);
                    return null;

                case 1:
                    if (DateTime.TryParse(busca, out dataIni))
                    {
                        dataIni = new DateTime(dataIni.Year, dataIni.Month, dataIni.Day);
                        dataFim = dataIni.AddDays(1);

                        return (DbQuery<Evento>)retorno.Where(c =>
                            c.DataCadastro >= dataIni &&
                            c.DataCadastro < dataFim
                        );
                    }
                    return null;

                case 2:
                    if (DateTime.TryParse(busca, out dataIni))
                    {
                        dataIni = new DateTime(dataIni.Year, dataIni.Month, dataIni.Day);
                        dataFim = dataIni.AddDays(1);

                        return (DbQuery<Evento>)retorno.Where(c =>
                            c.DataEvento >= dataIni &&
                            c.DataEvento < dataFim
                        );
                    }
                    return null;

                case 3:
                    if (int.TryParse(busca, out numeroBuscar))
                        return (DbQuery<Evento>)retorno.Where(c => c.QuantidadeDePessoas == numeroBuscar);
                    return null;

                case 4:
                    return (DbQuery<Evento>)retorno.Where(c => c.Responsavel.ToLower().Contains(busca.ToLower()));

                case 5:
                    return (DbQuery<Evento>)retorno.Where(c => c.Descricao.ToLower().Contains(busca.ToLower()));

                default:
                    int.TryParse(busca, out numeroBuscar);                    
                    if (DateTime.TryParse(busca, out dataIni))
                    {
                        dataIni = new DateTime(dataIni.Year, dataIni.Month, dataIni.Day);
                        dataFim = dataIni.AddDays(1);
                    }
                    return (DbQuery<Evento>)retorno.Where(c =>
                       c.Responsavel.ToLower().Contains(busca.ToLower()) ||
                       c.Descricao.ToLower().Contains(busca.ToLower()) ||
                       (numeroBuscar > 0 && (c.ID == numeroBuscar || c.QuantidadeDePessoas == numeroBuscar)) ||
                       (dataIni != default(DateTime) && (
                                (
                                    c.DataCadastro >= dataIni  &&
                                    c.DataCadastro < dataFim
                                ) || (
                                    c.DataEvento >= dataIni &&
                                    c.DataEvento < dataFim
                                )
                            )
                       )
                    );
            }
        }

        // GET: api/Eventos/5
        [ResponseType(typeof(Evento))]
        public IHttpActionResult GetEvento(int id)
        {
            Evento evento = db.Eventos.Find(id);
            if (evento == null)
            {
                return NotFound();
            }

            return Ok(evento);
        }

        // PUT: api/Eventos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEvento(int id, EventoViewModel evento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(getErrosValidacaoModelo(ModelState));
            }

            if (id != evento.ID)
            {
                return BadRequest();
            }

            Evento eventoModificado = Mapper.Map<Evento>(evento);
            db.Entry(eventoModificado).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Eventos
        [ResponseType(typeof(Evento))]
        public IHttpActionResult PostEvento(EventoViewModel evento)
        {
            evento.DataCadastro = DateTime.Now;

            if (!ModelState.IsValid)
            {
                return BadRequest(getErrosValidacaoModelo(ModelState));
            }

            Evento eventoNovo = new Evento();
            eventoNovo = Mapper.Map<Evento>(evento);
            db.Eventos.Add(eventoNovo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = eventoNovo.ID }, eventoNovo);
        }

        // DELETE: api/Eventos/5
        [ResponseType(typeof(Evento))]
        public IHttpActionResult DeleteEvento(int id)
        {
            Evento evento = db.Eventos.Find(id);
            if (evento == null)
            {
                return NotFound();
            }

            db.Eventos.Remove(evento);
            db.SaveChanges();

            return Ok(evento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventoExists(int id)
        {
            return db.Eventos.Count(e => e.ID == id) > 0;
        }

        private string getErrosValidacaoModelo(ModelStateDictionary modelState)
        {
            List<string> listaErros = new List<string>();

            var x = (from ModelState m in modelState.Values.AsEnumerable()
                     select (from m1 in m.Errors
                             where !string.IsNullOrWhiteSpace(m1.ErrorMessage)
                             select m1.ErrorMessage)
                    ).Where(m2 => m2.Count() > 0);

            foreach (IEnumerable<string> i in x)
                foreach (string i2 in i)
                    listaErros.Add(i2);

            return Newtonsoft.Json.JsonConvert.SerializeObject(listaErros);
        }

    }
}