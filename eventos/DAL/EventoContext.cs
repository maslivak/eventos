﻿using eventos.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace eventos.DAL
{
    public class EventoContext : DbContext
    {
        public EventoContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public EventoContext() : base("DefaultConnection") { }

        public DbSet<Evento> Eventos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    public class MigrationsContextFactory : IDbContextFactory<EventoContext>
    {
        public EventoContext Create()
        {
            return new EventoContext("DefaultConnection");
        }
    }

}