﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eventos.ViewModels
{
    public class EventoViewModel
    {
        [Display(Name = "ID")]
        public int? ID { get; set; }

        [Display(Name = "Data de Cadastro")]
        public DateTime? DataCadastro { get; set; }

        [Display(Name = "Data do Evento")]
        [DataType(DataType.Date, ErrorMessage = "Por favor informe uma data válida para {0}.")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.", AllowEmptyStrings = false)]
        [Range(typeof(DateTime),"01/01/1900", "01/01/9999", ErrorMessage = "Por favor informe uma data válida para {0}, maior que 01/01/1900 e menor que 01/01/9999.")]
        public DateTime? DataEvento { get; set; }

        [Display(Name = "Quantidade de Pessoas")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.", AllowEmptyStrings = false)]
        [Range(1, int.MaxValue, ErrorMessage = "O campo {0} deve ser maior que zero.")]
        public int? QuantidadeDePessoas { get; set; }

        [Display(Name = "Responsável")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [StringLength(100, ErrorMessage = "O campo {0} não deve exceder a {1} caracteres de comprimento.")]
        public string Responsavel { get; set; }

        [Display(Name = "Descrição")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [StringLength(4000, ErrorMessage = "O campo {0} não deve exceder a {1} caracteres de comprimento.")]
        public string Descricao { get; set; }
    }
}