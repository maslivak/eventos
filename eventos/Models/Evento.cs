﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eventos.Models
{
    public class Evento
    {
        public int ID { get; set; }
        
        [Required]
        public DateTime DataCadastro { get; set; }

        [Required]
        public DateTime DataEvento { get; set; }

        [Required]
        public int QuantidadeDePessoas { get; set; }

        [Required]
        [StringLength(100)]
        public string Responsavel { get; set; }
        
        [Required]
        [StringLength(4000)]
        public string Descricao { get; set; }
    }
}