namespace eventos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Requeridos : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Evento", "Responsavel", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Evento", "Descricao", c => c.String(nullable: false, maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Evento", "Descricao", c => c.String(maxLength: 4000));
            AlterColumn("dbo.Evento", "Responsavel", c => c.String(maxLength: 100));
        }
    }
}
