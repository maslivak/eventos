namespace eventos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Evento",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DataCadastro = c.DateTime(nullable: false),
                        DataEvento = c.DateTime(nullable: false),
                        QuantidadeDePessoas = c.Int(nullable: false),
                        Responsavel = c.String(maxLength: 100),
                        Descricao = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Evento");
        }
    }
}
